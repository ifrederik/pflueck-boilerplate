# Pflück.org Design Boilerplate

## pre-requisites

 * node (nodejs.org)
 * bower (npm install bower -g)
 * SASS (sass-lang.com/install)

## after checking out the repository

In the terminal, run:


```
#!sh

npm install
bower install
```

This will pull all dependencies (including the current source files for bootstrap).


# Designing the page

We are using the SASS version (getbootstrap.com/css/#sass) of the widley used Twitter Bootstrap template (getbootstrap.com). CSS files are not edited directly but always generated from the SCSS (located in the style folder). To do that, simply run grunt (it will watch for changed scss files and re-generate the CSS.)

## basic layout

To get started, simply start editing the index.html page located in the public folder.